-- MySQL Script generated by MySQL Workbench
-- Fri Sep 16 18:37:15 2022
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`usuario_Pac`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`usuario_Pac` (
  `idusuario_Pacientes` INT NOT NULL,
  `nombresapellidosusuario_pacientes` VARCHAR(45) NOT NULL,
  `email` INT NOT NULL,
  `numerocelular` VARCHAR(45) NOT NULL,
  `histclinusuario_Paci` VARCHAR(45) NOT NULL,
  `estadoclinico` TINYINT(2) NOT NULL,
  `usuario_Pac_Idusuario` INT NOT NULL,
  PRIMARY KEY (`idusuario_Pacientes`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`estados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`estados` (
  `idestado` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `descripcion` VARCHAR(45) NULL,
  PRIMARY KEY (`idestado`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`citas_medicas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`citas_medicas` (
  `idcitas_medicas` INT NOT NULL AUTO_INCREMENT,
  `descripcion_cita` VARCHAR(45) NULL,
  `usuario_pac_idusuario_Pacientes` INT NOT NULL,
  `estados_idestado` INT NOT NULL,
  PRIMARY KEY (`idcitas_medicas`),
  INDEX `fk_Citas_Médicas_Usuario_Pac_idx` (`usuario_pac_idusuario_Pacientes` ASC) VISIBLE,
  INDEX `fk_Citas_Médicas_Estados1_idx` (`estados_idestado` ASC) VISIBLE,
  CONSTRAINT `fk_Citas_Médicas_Usuario_Pac`
    FOREIGN KEY (`usuario_pac_idusuario_Pacientes`)
    REFERENCES `mydb`.`usuario_Pac` (`idusuario_Pacientes`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Citas_Médicas_Estados1`
    FOREIGN KEY (`estados_idestado`)
    REFERENCES `mydb`.`estados` (`idestado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
